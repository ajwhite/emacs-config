;;---------------------------------------------------------------
;; Personal defuns that don't depend on external packages
;;---------------------------------------------------------------

(defun ajw/autoformat-buffer ()
  "If there is no region selected, auto-indents the entire file and cleans up whitespace, otherwise
does the same to the selected region."
  (interactive)
  (if (region-active-p)
      (progn
        (indent-region (region-beginning) (region-end) nil)
        (whitespace-cleanup-region (region-beginning) (region-end)))
    (save-excursion
      (indent-region (point-min) (point-max) nil))
    (whitespace-cleanup)))

(defun ajw/diff-buffer-with-file ()
  (interactive)
  (if (buffer-file-name)
      (if (file-exists-p buffer-file-name)
          (diff-buffer-with-file)
        (message "File doesn't exist."))
    (message "Not in a file buffer.")))

(defun ajw/latex-count-words ()
  (interactive)
  (shell-command (concat "texcount "
                         (shell-quote-argument buffer-file-name)
                         " | grep 'Words in text'")))

(defun ajw/get-closest-pathname (file &optional dir)
  "Determine the pathname of the first instance of FILE starting from the current
directory towards root. DIR is an optional starting directory."
  (let ((dir (or dir "."))
        (root (expand-file-name "/")))
    (if (file-exists-p (expand-file-name file dir))
        (expand-file-name file dir)
      (unless (equal dir root)
        (ajw/get-closest-pathname file (expand-file-name ".." dir))))))

(defun ajw/make-clean ()
  (interactive)
  (let ((clean-compile-command (format "make -f %s clean" (ajw/get-closest-pathname "Makefile"))))
    (compile clean-compile-command)))

(defun ajw/insert-comment ()
  (interactive)
  "Insert a comment at point. If using evil-mode, enter insert state."
  (comment-normalize-vars)
  (insert (comment-padright comment-start))
  (save-excursion
    (unless (string= "" comment-end)
      (insert (comment-padleft comment-end))))
  (when (bound-and-true-p evil-mode)
    (evil-insert-state 1)))

(defun ajw/append-comment-eol ()
  (interactive)
  "Insert a comment at the end of line. If using evil-mode, enter insert state."
  ;; Going to use comment-dwim, so make sure region isn't active
  (deactivate-mark)
  (let ((comment-style 'multi-line))
    (call-interactively 'comment-dwim))
  (when (bound-and-true-p evil-mode)
    (evil-insert-state 1)))

(defun ajw/comment-dwim ()
  "Comment the region if it is active, otherwise comment the current line."
  (interactive)
  (comment-normalize-vars)
  (if (use-region-p)
      ;; Comment-or-uncomment whole region
      (comment-or-uncomment-region (region-beginning) (region-end))
    (let ((comment-style 'multi-line))
      (if (save-excursion (beginning-of-line) (looking-at "\\s-*$"))
          ;; Line is either empty or only whitespace
          (ajw/append-comment-eol)
        ;; Otherwise, comment-or-uncomment whole line
        (comment-or-uncomment-region (line-beginning-position) (line-end-position))))))

(defun ajw/time-of-day-compare (time-of-day &optional greater-than)
  "Compares the time specified to the current time."
  (if greater-than
      (time-less-p (current-time) (apply 'encode-time (append time-of-day (cdddr (decode-time)))))
    (time-less-p (apply 'encode-time (append time-of-day (cdddr (decode-time)))) (current-time))))

(defun ajw/split-window-below-and-focus ()
  "Split the window vertically and focus the new window."
  (interactive)
  (split-window-below)
  (windmove-down))

(defun ajw/split-window-right-and-focus ()
  "Split the window horizontally and focus the new window."
  (interactive)
  (split-window-right)
  (windmove-right))

(defun ajw/dos2unix ()
  (interactive)
  (set-buffer-file-coding-system 'undecided-unix))

(defun ajw/unix2dos ()
  (interactive)
  (set-buffer-file-coding-system 'undecided-dos))

(defun ajw/toggle-dos-eol (&optional show)
  (interactive)
  (setq buffer-display-table (make-display-table))
  (unless show
    (aset buffer-display-table ?\^M [])))

(defun ajw/delete-frame-or-kill-emacs ()
  "If connected to an Emacs server, kill the frame. If running a standalone
instance, kill Emacs."
  (interactive)
  (if (daemonp)
      (delete-frame)
    (kill-emacs)))
