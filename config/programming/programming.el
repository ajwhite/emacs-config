(defvar ajw/prog-mode-hook-alist
  '((python-mode . lsp))
  "An alist of major modes and associated prog-mode-hook functions")

(add-hook 'prog-mode-hook
          (lambda ()
            (unless (funcall (alist-get major-mode ajw/prog-mode-hook-alist 'ignore))
              (dumb-jump-mode)))
          90)

;;---------------------------------------------------------------
;; Programming-specific functions
;;---------------------------------------------------------------
(defun ajw/static-analyze (cmd)
  "Runs a static analysis tool and displays output in an Emacs compilation buffer.
CMD is the command to run against the current buffer file."
  (interactive)
  (let ((clang-tidy-compile-command (concat cmd " " (abbreviate-file-name (buffer-file-name))))
        (compilation-scroll-output 'first-error))
    (compile clang-tidy-compile-command)))

(defun ajw/autoformat-function ()
  "Auto-indents the current function and cleans up whitespace."
  (interactive)
  (save-excursion
    (mark-defun)
    (ajw/autoformat-buffer)
    (deactivate-mark)))


;;---------------------------------------------------------------
;; Packages used by all (or at least a few) programming modes
;;---------------------------------------------------------------
(general-create-definer ajw/general-prog-SPC-leader
  :states '(normal motion visual insert emacs)
  :keymaps '(prog-mode-map)
  :prefix "SPC"
  :non-normal-prefix "M-m")


;; Hide-Show minor mode
(add-hook 'prog-mode-hook 'hs-minor-mode)
(ajw/general-prog-SPC-leader
  "ch" 'hs-toggle-hiding
  "cH" 'hs-hide-all
  "cS" 'hs-show-all)
(use-package helm-xref
  :after popwin
  :ensure t
  :init
  (add-hook 'ajw/after-theme-change-functions
            (lambda (current-theme)
              (set-face-attribute 'helm-xref-file-name nil :foreground nil :inherit 'helm-grep-file)))
  :config
  (set-face-attribute 'helm-xref-file-name nil :foreground nil :inherit 'helm-grep-file)

  (if (< emacs-major-version 27)
      (progn
        (setq xref-show-xrefs-function #'helm-xref-show-xrefs)
        (setq xref-show-definitions-function #'helm-xref-show-xrefs))
    (setq xref-show-xrefs-function #'helm-xref-show-xrefs-27)
    (setq xref-show-definitions-function #'helm-xref-show-xrefs-27))

  (setq helm-xref-candidate-formatting-function 'helm-xref-format-candidate-long)
  (add-to-list 'xref-prompt-for-identifier 'xref-find-references t)
  (push "*helm-xref*" popwin:special-display-config))

(use-package lsp-mode
  :ensure t
  :init
  (setq read-process-output-max (* 1024 1024)) ;; 1MB

  (setq lsp-auto-configure       t
        lsp-diagnostic-package   :none
        lsp-keep-workspace-alive nil ; Don't keep language server running after all associated buffers are closed
        lsp-enable-indentation   nil
        lsp-auto-guess-root      nil
        lsp-before-save-edits    nil ; Why would you even want this??
        lsp-enable-snippet       t
        lsp-file-watch-threshold nil
        lsp-keymap-prefix        nil
        lsp-enable-xref          t
        lsp-enable-imenu         t
        lsp-enable-completion-at-point nil
        lsp-enable-on-type-formatting  nil) ; Native Emacs formatting is sufficient

  :config
  (ajw/general-prog-SPC-leader
    "l"  '(:ignore t :which-key "lsp")
    "l?" '(xref-find-references  :which-key "find-references")
    "ld" '(xref-find-definitions :which-key "find-definition")
    "l." '(xref-find-definitions :which-key "find-definition")
    "l," '(xref-pop-marker-stack :which-key "pop-location-stack")
    "lr" '(lsp-rename            :which-key "rename-under-point"))
  )

;;Company -- pop-up completin
(use-package company
  :ensure t
  :commands
  (company-mode)

  :init
  ;; Needed for inserting completions correctly
  (use-package yasnippet
    :ensure t
    :hook
    (company-mode . yas-minor-mode))

  ;; Show pop-up in child frame
  (use-package company-posframe
    :ensure t
    :hook
    (company-mode . company-posframe-mode))

  :config
  (setq company-idle-delay nil) ; No idle completion

  :hook
  (lsp-mode . company-mode)

  :general
  (:keymaps 'company-mode-map "<backtab>" 'company-complete))

;; Company backend for lsp-mode
(use-package company-lsp
  :ensure t
  :config
  (push 'company-lsp company-backends)
  (setq company-transformers nil)
  (setq company-lsp-async t)
  (setq company-lsp-cache-candidates nil))

;;---------------------------------------------------------------
;; Language-specific support/configuration
;;---------------------------------------------------------------
;; Shell-script-mode
(general-define-key
 :states '(normal motion visual insert emacs)
 :keymaps 'sh-mode-map
 :prefix "SPC"
 :non-normal-prefix "M-m"
 "sc" '((lambda()
          (interactive) (ajw/static-analyze "shellcheck -Cnever"))
        :which-key "shellcheck"))

;; Rust
(use-package rust-mode
  :ensure t
  :init
  (defun ajw/rust-mode-init ()
    (when (ajw/get-closest-pathname "Cargo.toml")
      (lsp)))

  (add-to-list 'ajw/prog-mode-hook-alist '(rust-mode . ajw/rust-mode-init)))
