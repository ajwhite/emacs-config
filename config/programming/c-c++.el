;; C/C++ mode-specific SPC leader key
(general-create-definer ajw/general-c-c++-SPC-leader
  :states '(normal motion visual insert emacs)
  :keymaps '(c-mode-map c++-mode-map)
  :prefix "SPC"
  :non-normal-prefix "M-m")

;; C/C++ specific keybindings
(ajw/general-c-c++-SPC-leader
  ;;===================================================
  "c" '(:ignore t :which-key "comment/C/C++/compilation")
  "cc" 'compile
  "cC" '(ajw/make-clean                     :which-key "make-clean")
  "cd" '(spacemacs/close-compilation-window :which-key "close-compilation-window")
  "cj" '(c-end-of-defun                     :which-key "end-of-function")
  "ck" '(c-beginning-of-defun               :which-key "beginning-of-function")
  "cK" 'kill-compilation
  "cm" '(mark-defun                         :which-key "mark-function")
  "cn" '(c-display-defun-name               :which-key "display-function-name")
  "cr" 'recompile
  "ct" '((lambda() (interactive)
           (ajw/static-analyze "clang-tidy")) :which-key "clang-tidy")
  )

;;Language Server Protocol backend for C and C++
(use-package ccls
  :ensure t
  :after projectile
  :init
  (setq ccls-executable "/usr/bin/ccls")
  (setq ccls-initialization-options '(:completion (:detailedLabel t) :index (:comments 0)))

  (defun ajw/ccls-init ()
    (setq-local ccls-args (list (concat "--log-file=/tmp/ccls.log." (ajw/project-path-to-filename)))))

  (defun c-c++-lsp-enable ()
    (if (and (projectile-project-p) (file-exists-p (concat (projectile-project-root) "compile_commands.json")))
        (progn
          (ajw/ccls-init)
          (lsp))
      (message "No compile_commands.json found in project root: LSP server not started")
      nil))

  (add-to-list 'ajw/prog-mode-hook-alist '(c-mode . c-c++-lsp-enable))
  (add-to-list 'ajw/prog-mode-hook-alist '(c++-mode . c-c++-lsp-enable))

  :config
  (setq projectile-globally-ignored-directories
        (append '(".ccls-cache") projectile-globally-ignored-directories))

  (defun ccls/callee  () (interactive) (lsp-find-custom "$ccls/call" '(:callee t)))
  (defun ccls/caller  () (interactive) (lsp-find-custom "$ccls/call"))
  (defun ccls/base    () (interactive) (lsp-find-custom "$ccls/inheritance" '(:levels 1)))
  (defun ccls/derived () (interactive) (lsp-find-custom "$ccls/inheritance" '(:levels 1 :derived t)))
  (defun ccls/member (kind) (interactive) (lsp-find-custom "$ccls/member" `(:kind ,kind)))

  (ajw/general-c-c++-SPC-leader
    "lc"  '(:ignore t    :which-key "ccls")
    "lcb" '(ccls/base    :which-key "find-base-class")
    "lcc" '(ccls/callee  :which-key "find-callee")
    "lcC" '(ccls/caller  :which-key "find-caller")
    "lcd" '(ccls/derived :which-key "find-derived-class")
    "lcr" 'ccls-reload)
  )
