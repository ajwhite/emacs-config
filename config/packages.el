;;---------------------------------------------------------------
;; Packages common to both GUI and terminal
;;---------------------------------------------------------------

;; Keybindings
(use-package general
  :ensure t
  :demand
  :config
  (general-auto-unbind-keys)

  ;; Global SPC leader key
  (general-create-definer ajw/general-global-SPC-leader
    :states '(normal motion visual insert emacs)
    :prefix "SPC"
    :non-normal-prefix "M-m")

  ;; All programming modes SPC leader key
  (general-create-definer ajw/general-prog-mode-SPC-leader
    :prefix "SPC"
    :non-normal-prefix "M-m"
    :states '(normal motion visual insert emacs)
    :keymaps 'prog-mode-map))

;;Evil Mode
(use-package evil
  :ensure t
  :demand
  :commands
  (evil-mode)

  :init
  (setq evil-disable-insert-state-bindings t)
  (setq evil-toggle-key "C-`")
  (add-hook 'ajw/after-theme-change-functions (lambda (current-theme)
                                                (setq
                                                 evil-insert-state-cursor (list 'bar ajw/evil-insert-state-color)
                                                 evil-emacs-state-cursor  (list 'box ajw/evil-insert-state-color)
                                                 evil-motion-state-cursor (list 'box ajw/evil-motion-state-color)
                                                 evil-normal-state-cursor (list 'box ajw/evil-normal-state-color)
                                                 evil-visual-state-cursor (list 'box ajw/evil-visual-state-color))))

  (defun ajw/evil-newline-below (count)
    "Insert a new line below point.
The insertion will be repeated COUNT times."
    (interactive "p")
    (unless (eq evil-want-fine-undo t)
      (evil-start-undo-step))
    (push (point) buffer-undo-list)
    (evil-insert-newline-below)
    (setq evil-insert-count count
          evil-insert-lines t
          evil-insert-vcount nil))

  (defun ajw/evil-next-visual-or-logical-line (count)
    "Move the cursor to the next visual line if used without prefix argument.
Move down COUNT logical lines if used with prefix argument."
    (interactive "p")
    (if (/= count 1)
        (let (line-move-visual)
          (evil-beginning-of-line) ; Otherwise we end up in weird places
          (evil-line-move count))
      (let ((line-move-visual t))
        (evil-line-move 1))))

  (defun ajw/evil-previous-visual-or-logical-line (count)
    "Move the cursor to the previous visual line if used without prefix argument.
Move up COUNT logical lines if used with prefix argument."
    (interactive "p")
    (if (/= count 1)
        (let (line-move-visual)
          (evil-beginning-of-line) ; Otherwise we end up in weird places
          (evil-line-move (- count)))
      (let ((line-move-visual t))
        (evil-line-move -1))))

  (defun ajw/evil-beginning-end-indented-visual-line ()
    (interactive)
    "Move to the beginning of indent first time - end of visual line second time"
    (if (eq this-command last-command)
        (if (or (equal (point) (point-at-eol))
                (equal (point) (- (point-at-eol) 1))) ; Evil doesn't quite put the point at the eol
            (evil-first-non-blank-of-visual-line)
          (evil-end-of-visual-line))
      (evil-first-non-blank-of-visual-line)))

  :general
  (:states 'normal
           "<backspace>" 'backward-delete-char-untabify
           "RET" 'ajw/evil-newline-below
           "L"   'move-to-window-line-top-bottom

           "j"   'ajw/evil-next-visual-or-logical-line
           "k"   'ajw/evil-previous-visual-or-logical-line

           "gh"  'beginning-of-line
           "gj"  'end-of-buffer
           "gk"  'beginning-of-buffer
           "gl"  'end-of-line

           "q"   'ajw/evil-beginning-end-indented-visual-line
           "Q"   'evil-jump-item)

  :config
  ;; Use normal keybindings in insert mode, except for C-o
  (evil-define-key 'insert global-map (kbd "C-o") 'evil-execute-in-normal-state)

  (defalias 'forward-evil-WORD 'forward-evil-symbol)
  (add-to-list 'evil-motion-state-modes 'package-menu-mode)
  (add-to-list 'evil-motion-state-modes 'messages-buffer-mode)
  (add-to-list 'evil-motion-state-modes 'diff-mode)

  (setq evil-normal-state-tag   "NORMAL")
  (setq evil-insert-state-tag   "INSERT")
  (setq evil-visual-state-tag   "VISUAL")
  (setq evil-emacs-state-tag    "EMACS")
  (setq evil-motion-state-tag   "MOTION")
  (setq evil-operator-state-tag "OPERATOR")
  (setq evil-replace-state-tag  "REPLACE")

  ;;Use "jk" to escape from insert mode
  (use-package evil-escape
    :ensure t
    :config
    (setq evil-escape-delay '0.20)
    (setq-default evil-escape-key-sequence "jk")
    (setq evil-escape-unordered-key-sequence t)
    (evil-escape-mode))

  (use-package evil-magit
    :ensure t
    :after magit
    :init
    (evil-magit-init)

    :config
    (define-key magit-mode-map (kbd "SPC") nil) ;; Don't overwrite SPC leader in magit
    (add-to-list 'evil-escape-excluded-major-modes 'magit-status-mode)
    (add-to-list 'evil-motion-state-modes 'magit-blame-mode))

  (use-package evil-surround
    :ensure t
    :init
    (global-evil-surround-mode 1)

    :general
    (:keymaps 'evil-surround-mode-map :states 'visual
              "s" 'evil-surround-region
              "S" 'evil-substitute))

  (evil-mode 1))

;;Swiper
(use-package swiper
  :ensure t
  :config
  (defun ajw/swiper-dwim ()
    "Use the current active region as an argument to swiper. If the region isn't active,
the current symbol under the point is used."
    (interactive)
    (let ((search-symbol (if (region-active-p)
                             (buffer-substring-no-properties (region-beginning) (region-end))
                           (when (symbol-at-point)
                             (symbol-name (symbol-at-point))))))
      (deactivate-mark)
      (swiper search-symbol)))

  (ajw/general-global-SPC-leader
    "ss" 'swiper
    "sS" '(ajw/swiper-dwim :which-key "swiper-dwim")))


(use-package counsel
  :ensure t
  :config
  (setq counsel-find-file-speedup-remote nil) ; Run find-file-hook on remote files, too
  (defun ajw/find-config-file (&optional path)
    (interactive)
    (if path
        (counsel-find-file path)
      (counsel-find-file "~/.emacs.d/config/")))

  (defun ajw/find-file-home-dir ()
    (interactive)
    (counsel-find-file "~/"))

  ;; Use counsel-M-x but without any initial input
  (global-set-key (kbd "M-x") (lambda () (interactive) (counsel-M-x "")))

  (define-key ivy-minibuffer-map (kbd "C-j")   'ivy-immediate-done)
  (define-key ivy-minibuffer-map (kbd "C-M-j") 'ivy-alt-done)

  (ajw/general-global-SPC-leader
    "fc" '(ajw/find-config-file :which-key "find-config-file")
    "fh" '(ajw/find-file-home-dir :which-key "find-file-home-dir")
    "ff" '(counsel-find-file :which-key "find-file")
    ))

;;Helm
(use-package helm
  :ensure t
  :bind
  (:map helm-map
        ("<tab>" . helm-execute-persistent-action)
        ("C-z" . helm-select-action))

  :config
  (setq helm-no-headers nil)

  (ajw/general-global-SPC-leader
    "ha" 'helm-apropos
    "hr" 'helm-resume
    "hR" 'helm-register)

  (ajw/general-prog-mode-SPC-leader
    "ci" 'helm-imenu)

  ;;Helm frontend for The Silver Searcher
  (use-package helm-ag
    :ensure t))

;;which-key
(use-package which-key
  :ensure t
  :config
  (setq which-key-sort-order 'which-key-local-then-key-order)
  (setq which-key-idle-delay 0.5)
  (which-key-mode))

;;Magit
(use-package magit
  :ensure t
  :config
  ;; Try to put Magit buffer in a vertical column as often as possible
  (setq magit-display-buffer-function 'magit-display-buffer-fullcolumn-most-v1)

  (ajw/general-global-SPC-leader
    "g" '(:ignore t :which-key "git")
    "gg" 'magit-status
    "gb" 'magit-blame
    "gl" 'magit-log-buffer-file))

(use-package projectile
  :ensure t
  :config
  (setq projectile-enable-caching t)
  (setq projectile-indexing-method 'hybrid)
  (setq projectile-globally-ignored-files (append '("compile_commands.json") projectile-globally-ignored-files))
  (projectile-global-mode)

  (defun ajw/project-path-to-filename ()
    (substring
     (replace-regexp-in-string "\\/" "_" (projectile-project-root))
     1 -1))

  (use-package helm-projectile
    :ensure t
    :init
    (defun ajw/helm-projectile-ag-symbol-at-point ()
      (interactive)
      (let ((helm-ag-insert-at-point 'symbol))
        (helm-projectile-ag)))))

(use-package popwin
  :ensure t
  :config
  (delete '(compilation-mode :noselect t) popwin:special-display-config) ;;Disable popwin for compilation mode
  (popwin-mode 1))

(use-package hl-todo
  :ensure t
  :hook
  (prog-mode . hl-todo-mode))

;; Only highlight words in comments, not strings
(eval-after-load "hl-todo"
  '(defun hl-todo--search (&optional regexp bound backward)
     (unless regexp
       (setq regexp hl-todo--regexp))
     (and (let ((case-fold-search nil))
            (with-syntax-table hl-todo--syntax-table
              (funcall (if backward #'re-search-backward #'re-search-forward)
                       regexp bound t)))
          (or (apply #'derived-mode-p hl-todo-text-modes)
              (nth 4 (syntax-ppss)) ; inside comment
              (and (or (not bound)
                       (funcall (if backward #'< #'>) bound (point)))
                   (hl-todo--search regexp bound backward))))))

(use-package visual-regexp-steroids
  :ensure t
  :defer t)

(use-package avy
  :ensure t
  :after evil
  :init
  (setq avy-all-windows 'all-frames)
  (setq avy-background t)

  (ajw/general-global-SPC-leader
    "j"  '(:ignore t :which-key "jump")
    "jb" 'avy-pop-mark
    "jj" 'evil-avy-goto-char
    "jJ" 'evil-avy-goto-char-2
    "jl" 'evil-avy-goto-line
    "jw" 'evil-avy-goto-word-or-subword-1))

;; Garbage Collector Magic Hack
(use-package gcmh
  :ensure t
  :config
  (gcmh-mode 1))
