;; https://emacs.stackexchange.com/a/37270
(defun simple-mode-line-render (left right)
  "Return a string of `window-width' length containing LEFT, and RIGHT aligned respectively."
  (let* ((available-width (- (window-total-width) (+ (length (format-mode-line left)) (length (format-mode-line right))))))
    (append left (list (format (format "%%%ds" available-width) "")) right)))

(defun ajw/bold (text)
  `(:propertize ,text face (:weight bold)))

;; Segments
(setq ml-end-spc
      '(:eval
        (if (display-graphic-p)
            " "
          "-%-")))

(setq ajw/lsp-mode-line
      '(:eval
        (when (bound-and-true-p lsp-mode)
          (concat (lsp-mode-line) " "))))

(setq ajw/git-mode-line
      '(:eval
        (when (eq 'Git (vc-backend buffer-file-name))
          (propertize (substring vc-mode 1) 'face '(:inherit (success bold))))))

(setq ajw/buffer-mode-line
      '(:eval
        (propertize
         (if (buffer-file-name)
             (concat
              (propertize (file-name-directory (abbreviate-file-name buffer-file-name))
                          'face '(font-lock-comment-face :weight bold))
              (propertize (buffer-name)
                          'face '(font-lock-string-face :weight bold)))
           (propertize (buffer-name) 'face '(:weight bold)))
         'help-echo "mouse-1: Previous buffer\nmouse-3: Next buffer"
         'local-map mode-line-buffer-identification-keymap
         'mouse-face 'mode-line-highlight)))

(setq ajw/major-mode
      `(:propertize
        "%m"
        face (:weight bold)
        mouse-face mode-line-highlight
        local-map ,mode-line-major-mode-keymap
        help-echo "Major mode
mouse-1: Display major mode menu
mouse-2: Show help for major mode
mouse-3: Toggle minor modes"))

(setq ajw/evil-mode-line
      '(:eval
        (when (bound-and-true-p evil-local-mode)
          (concat
           (propertize
            evil-mode-line-tag
            'face `(:weight bold :foreground
                            ,(cond ((evil-normal-state-p)   ajw/evil-normal-state-color)
                                   ((evil-emacs-state-p)    ajw/evil-insert-state-color)
                                   ((evil-insert-state-p)   ajw/evil-insert-state-color)
                                   ((evil-motion-state-p)   ajw/evil-motion-state-color)
                                   ((evil-visual-state-p)   ajw/evil-visual-state-color)
                                   ((evil-operator-state-p) ajw/evil-normal-state-color)
                                   ((evil-replace-state-p)  ajw/evil-normal-state-color))))
           " "))))

(setq-default mode-line-format
              '((:eval
                 (simple-mode-line-render
                  ;; Left side
                  (list
                   "%e"                    ; Emacs memory error
                   mode-line-front-space
                   ajw/evil-mode-line      ; Evil state indicator
                   "[" (ajw/bold "%*") "]" ; Buffer status
                   " "
                   ajw/buffer-mode-line    ; Buffer/File name
                   " "
                   "%l:%c"                 ; Line number:Column number
                   " "
                   "%p"                    ; Buffer position
                   )

                  ;; Right side
                  (list
                   ajw/lsp-mode-line       ; LSP info
                   ajw/major-mode          ; Major mode
                   " "
                   ajw/git-mode-line       ; Git info
                   " "
                   ml-end-spc
                   )))))
