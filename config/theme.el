;;---------------------------------------------------------------
;; Theme configurations for all frames
;;---------------------------------------------------------------

;; Light theme
(setq light-theme-name 'solarized-light)
(use-package solarized-theme
  :ensure t
  :pin melpa
  :defer t
  :init
  (require 'solarized)
  (setq
   light-theme-evil-insert-state-color "#268BD2"
   light-theme-evil-motion-state-color "#D33682"
   light-theme-evil-normal-state-color "#E1AF4B"
   light-theme-evil-visual-state-color "#CCCEC4")
  (add-hook 'ajw/after-theme-change-functions
            (lambda (current-theme)
              (when (eql current-theme light-theme-name)
                (set-face-attribute 'mode-line-inactive nil :underline nil :overline nil
                                    :box `(:line-width 1 :color "#CCCEC4"))
                (set-face-attribute 'mode-line nil :underline nil :overline nil
                                    :box `(:line-width 1 :color "#CCCEC4")))))

  (with-eval-after-load "lsp"
    (add-hook 'ajw/after-theme-change-functions
              (lambda (current-theme)
                (when (eql current-theme light-theme-name)
                  (set-face-attribute 'lsp-face-highlight-textual nil :background "#EEE8D5"
                                      :distant-foreground ajw/evil-insert-state-color :bold t)
                  (set-face-attribute 'lsp-face-highlight-read nil :foreground nil :background nil :inherit 'diff-refine-removed :bold t)
                  (set-face-attribute 'lsp-face-highlight-write nil :foreground nil :background nil :inherit 'diff-refine-added :bold t)))))
  )

;; Dark theme
(setq dark-theme-name 'darktooth)
(use-package darktooth-theme
  :ensure t
  :defer t
  :init
  (setq
   dark-theme-evil-insert-state-color  "#83A598"
   dark-theme-evil-motion-state-color  "#D3869B"
   dark-theme-evil-normal-state-color  "#FABD2F"
   dark-theme-evil-visual-state-color  "#CCCEC4")
  (add-hook 'ajw/after-theme-change-functions
            (lambda (current-theme)
              (when (eql current-theme dark-theme-name)
                (set-face-attribute 'mode-line-inactive nil :box '(:line-width 1 :color "#504945"))
                (set-face-attribute 'mode-line nil :box '(:line-width 1 :color "#504945"))
                (mapcar
                 (lambda (face) (set-face-attribute face nil :extend t))
                 '(hl-line
                   region
                   magit-section-highlight
                   magit-diff-context-highlight
                   magit-diff-added
                   magit-diff-added-highlight
                   magit-diff-removed
                   magit-diff-removed-highlight)))))

  (with-eval-after-load "lsp"
    (add-hook 'ajw/after-theme-change-functions
              (lambda (current-theme)
                (when (eql current-theme dark-theme-name)
                  (set-face-attribute 'lsp-face-highlight-textual nil :background nil :foreground nil :bold t :inherit 'highlight)
                  (set-face-attribute 'lsp-face-highlight-read    nil :background nil :foreground nil :bold t :inherit 'highlight)
                  (set-face-attribute 'lsp-face-highlight-write   nil :background nil :foreground nil :bold t :inherit 'highlight)))))
  )

;; Terminal theme
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "lisp/"))
(setq terminal-theme-name 'tty-dark)


;;---------------------------------------------------------------
;; Theme change logic
;;---------------------------------------------------------------

;;This package provides functions that use your location to determine if it
;;should set a light theme or dark theme. (based on sunrise/sunset)
;;I don't like the inflexibilty of theme-changer's change-theme function, so I
;;use theme-changer's internal functions to implement my own.
(use-package theme-changer
  :ensure t
  :pin melpa
  )

(defvar ajw/after-theme-change-functions nil
  "List of functions to run after theme change. Functions must take the current
theme name as an argument")

;; These variables are set by the theming functions
(defvar ajw/evil-insert-state-color nil "Color to use when in evil insert state.")
(defvar ajw/evil-normal-state-color nil "Color to use when in evil normal state.")
(defvar ajw/evil-motion-state-color nil "Color to use when in evil motion state.")
(defvar ajw/evil-visual-state-color nil "Color to use when in evil visual state.")
(defvar ajw/file-name-color nil "Color to use for file names.")
(defvar ajw/current-theme nil "The current theme.")

(require 'cl-macs)
(defun ajw/emacs-theme-function (&rest args)
  "Sets custom theme and evil color variables to either a light theme or dark
theme, depending on the time of day. Setting ajw/theme-override will force
either dark or light theme."
  (interactive)
  ;; Use theme-override selection if available
  (if (boundp 'ajw/theme-override)
      (theme-changer-switch-theme ajw/current-theme ajw/theme-override)
    ;; Else use theme-changer.el to get sunrise/sunset times and use that
    (let ((now (current-time)))
      (cl-destructuring-bind (sunrise-today sunset-today)
          (theme-changer-sunrise-sunset-times (theme-changer-today))
        (if (or (time-less-p now sunrise-today) (time-less-p sunset-today now))
            (theme-changer-switch-theme ajw/current-theme dark-theme-name)
          (theme-changer-switch-theme ajw/current-theme light-theme-name)))))

  ;; Set evil color variables
  (cond
   ((eql ajw/current-theme dark-theme-name)
    (setq
     ajw/evil-insert-state-color dark-theme-evil-insert-state-color
     ajw/evil-normal-state-color dark-theme-evil-normal-state-color
     ajw/evil-motion-state-color dark-theme-evil-motion-state-color
     ajw/evil-visual-state-color dark-theme-evil-visual-state-color))
   ((eql ajw/current-theme light-theme-name)
    (setq
     ajw/evil-insert-state-color light-theme-evil-insert-state-color
     ajw/evil-normal-state-color light-theme-evil-normal-state-color
     ajw/evil-motion-state-color light-theme-evil-motion-state-color
     ajw/evil-visual-state-color light-theme-evil-visual-state-color))
   (t ;; Default colors
    (setq
     ajw/evil-insert-state-color (face-attribute 'cursor :background nil t)
     ajw/evil-normal-state-color (face-attribute 'cursor :background nil t)
     ajw/evil-motion-state-color (face-attribute 'cursor :background nil t)
     ajw/evil-visual-state-color (face-attribute 'cursor :background nil t))))

  (run-hook-with-args 'ajw/after-theme-change-functions ajw/current-theme))

;; Set ajw/current-theme variable every time the theme changes
(defadvice load-theme (after ajw/load-theme-advice activate)
  (let ((theme (ad-get-arg 0)))
    (setq ajw/current-theme theme)))


(defun ajw/enable-theme ()
  (if (daemonp)
      (progn
        (defadvice server-create-tty-frame (after ajw/tty-frame-advice activate)
          (ajw/switch-theme terminal-theme-name))
        (defadvice server-create-window-system-frame (after ajw/window-system-frame-advice activate)
          (ajw/emacs-theme-function)))

    ;; Not running as daemon
    (unless (display-graphic-p)
      (setq ajw/theme-override terminal-theme-name))

    (ajw/emacs-theme-function)))


(defun ajw/switch-theme (new-theme &rest args)
  "Set the theme to NEW-THEME and disable day/night theme auto-change."
  (interactive
   (list
    (intern (completing-read "Load theme: "
                             (mapcar #'symbol-name
                                     (custom-available-themes))))
    nil nil))
  (setq ajw/theme-override new-theme)
  (ajw/emacs-theme-function)
  (message "Current theme: %s" ajw/current-theme))

(defun ajw/reset-theme ()
  "Restore theme to custom default and enable day/night theme auto-change."
  (interactive)
  (makunbound 'ajw/theme-override)
  (ajw/emacs-theme-function)
  (message "Current theme: %s" ajw/current-theme))

(defun ajw/disable-theme ()
  "Set theme to Emacs default and disable day/night theme auto-change."
  (interactive)
  (setq ajw/theme-override nil)
  (ajw/emacs-theme-function)
  (setq ajw/current-theme nil)
  (ajw/emacs-theme-function))

;; Theme keybindings
(ajw/general-global-SPC-leader
  "T" '(:ignore t :which-key "theme")
  "TD" '(ajw/disable-theme :which-key "disable-theme")
  "Ts" '(ajw/switch-theme :which-key "switch-theme")
  "Tr" '(ajw/reset-theme  :which-key "reset-theme")
  "Td" '((lambda () (interactive) (ajw/switch-theme dark-theme-name)) :which-key "dark-theme")
  "Tl" '((lambda () (interactive) (ajw/switch-theme light-theme-name)) :which-key "light-theme"))
