;;Global keybindings ===========================================================
(general-define-key
 :states '(normal motion visual insert emacs)

 ;;Allow moving 5 lines at a time
 "M-n" (lambda () (interactive) (forward-line  5))
 "M-p" (lambda () (interactive) (forward-line  -5))

 ;;Map custom auto-indent function
 "<f5>" 'ajw/autoformat-buffer

 ;;Replace commands with dwim counterparts
 "M-u" 'upcase-dwim
 "M-l" 'downcase-dwim
 "M-c" 'capitalize-dwim

 "M-RET" 'indent-new-comment-line

 ;;Change text scale with mouse wheel
 "<C-mouse-4>" 'text-scale-increase
 "<C-mouse-5>" 'text-scale-decrease

 "M-." 'xref-find-definitions
 "M-," 'xref-pop-marker-stack

 "TAB" 'indent-for-tab-command
 )

;;Evil keybindings ==============================================================
;;Spacemacs-style prefix commands
(ajw/general-global-SPC-leader

  "u" 'universal-argument
  "SPC" 'counsel-M-x

  ;;===================================================
  "a" '(:ignore t :which-key "applications")
  "ac"  'calc-dispatch
  "ap"  'list-processes
  "ag"  '(helm-google-suggest :which-key "google")
  "aP"  'proced
  "au"  'undo-tree-visualize
  "am"  '(helm-man-woman :which-key "man")

  "ao"  '(:ignore t :which-key "org")
 "aoc" 'org-capture

 "as"  '(:ignore t :which-key "shell")
 "as!" 'shell-command
 "ass" 'shell-command
 "as|" 'shell-command-on-region
 "asS" 'shell-command-on-region
 "asi" '((lambda () (interactive) (let ((current-prefix-arg '(4))) (call-interactively 'shell-command))) :which-key "shell-command-insert-output")
 "asI" '((lambda () (interactive) (let ((current-prefix-arg '(4))) (call-interactively 'shell-command-on-region))) :which-key "shell-command-on-region-replace-output")
 "asa" 'async-shell-command

 ;;===================================================
 "b" '(:ignore t :which-key "buffers")
 "b TAB" '(spacemacs/alternate-buffer :which-key "alternate-buffer")
 "bb"    '(ivy-switch-buffer :which-key "select-buffer")
 "bd"    'kill-this-buffer
 "bD"    'kill-buffer-and-window
 "be"    '(spacemacs/safe-erase-buffer :which-key "safe-erase-buffer")
 "bf"    '(ajw/diff-buffer-with-file :which-key "diff-buffer-with-file")
 "b C-d" '(spacemacs/kill-matching-buffers-rudely :which-key "kill-matching-buffers-rudely")
 "bi"    '(ajw/autoformat-buffer :which-key "format-buffer")
 "bk"    'kill-buffer
 "bn"    'next-buffer
 "bm"    '(spacemacs/kill-other-buffers :which-key "kill-other-buffers")
 "bp"    'previous-buffer
 "br"    'revert-buffer
 "bY"    '(spacemacs/copy-whole-buffer-to-clipboard :which-key "copy-whole-buffer-to-clipboard")
 "bw"    'read-only-mode

 ;;===================================================
 "f" '(:ignore t :which-key "files")
 "fg" 'rgrep
 "fl" 'find-file-literally
 "fR" '(spacemacs/rename-current-buffer-file :which-key "rename-current-buffer-file")
 "fr" 'helm-recentf
 "fS" 'write-file
 "fs" 'save-buffer
 "fy" '(spacemacs/show-and-copy-buffer-filename :which-key "show-and-copy-buffer-filename")

 "fC" '(:ignore t :which-key "convert")
 "fCd" '(ajw/unix2dos :which-key "unix2dos")
 "fCu" '(ajw/dos2unix :which-key "dos2unix")

 "fv" '(:ignore t :which-key "variables")
 "fvd" 'add-dir-local-variable
 "fvf" 'add-file-local-variable
 "fvp" 'add-file-local-variable-prop-line

 ;;===================================================
 "h" '(:ignore t :which-key "help")
 "ht" 'help-with-tutorial
 "hn" 'view-emacs-news

 "hd" '(:ignore t :which-key "describe")
 "hdb" 'describe-bindings
 "hdc" 'describe-char
 "hdf" 'describe-function
 "hdF" 'describe-face
 "hdk" 'describe-key
 "hdp" 'describe-package
 "hdt" 'describe-theme
 "hdm" 'describe-mode
 "hdv" 'describe-variable

 ;;===================================================
 "m" '(:ignore t :which-key "macros")
 "mm" '(kmacro-start-macro-or-insert-counter :which-key "start-macro-or-insert-counter")
 "mM" '(kmacro-end-or-call-macro  :which-key "end-or-call-macro")
 "mv" '(kmacro-view-macro-repeat  :which-key "view-macro")

 "mc" '(:ignore t :which-key "counter")
 "mca" '(kmacro-add-counter       :which-key "add-counter")
 "mcc" '(kmacro-insert-counter    :which-key "insert-counter")
 "mcC" '(kmacro-set-counter       :which-key "set-counter")
 "mcf" '(kmacro-set-format        :which-key "set-format")

 "me" '(:ignore t :which-key "edit")
 "mee" '(kmacro-edit-macro-repeat :which-key "edit-macro")
 "mel" '(kmacro-edit-lossage      :which-key "edit-lossage")
 "men" '(kmacro-name-last-macro   :which-key "name-last-macro")
 "mer" '(kmacro-to-register       :which-key "to-register")
 "mes" '(kmacro-step-edit-macro   :which-key "step-edit-macro")

 ;;===================================================
 "n" '(:ignore t :which-key "narrow")
 "nr" 'narrow-to-region
 "np" 'narrow-to-page
 "nf" 'narrow-to-defun
 "nw" 'widen

 ;;===================================================
 "p" '(:ignore t :which-key "projects")
 "pa" 'helm-projectile-ag
 "pA" '(ajw/helm-projectile-ag-symbol-at-point :which-key "helm-projectile-ag-symbol-at-point")
 "pb" 'helm-projectile-switch-to-buffer
 "pd" 'helm-projectile-find-dir
 "pf" 'helm-projectile-find-file
 "ph" 'helm-projectile
 "pp" 'helm-projectile-switch-project
 "pr" 'helm-projectile-recentf
 "pv" 'projectile-vc
 "p!" 'projectile-run-shell-command-in-root
 "p&" 'projectile-run-async-shell-command-in-root
 "p%" 'projectile-replace-regexp
 "pc" 'projectile-compile-project
 "pD" 'projectile-dired
 "pI" 'projectile-invalidate-cache
 "pk" 'projectile-kill-buffers
 "pR" 'projectile-replace
 "pT" 'projectile-test-project
 "po" 'projectile-find-other-file

 ;;===================================================
 "q" '(:ignore t :which-key "quit")
 "qq" '(ajw/delete-frame-or-kill-emacs :which-key "delete-frame-or-kill-emacs")
 "qs" 'save-buffers-kill-emacs
 "qQ" 'kill-emacs

 ;;===================================================
 "r" '(:ignore t :which-key "registers")
 "ri" 'insert-register
 "rj" 'jump-to-register

 "rp" '(:ignore t :which-key "position")
 "rpp" 'point-to-register

 "rt" '(:ignore t :which-key "text")
 "rtc" 'copy-to-register
 "rtt" 'copy-to-register
 "rta" 'append-to-register
 "rtp" 'prepend-to-register

 "rn" '(:ignore t :which-key "number")
 "rnn" 'number-to-register
 "rn+" 'increment-register

 "rw" '(:ignore t :which-key "window")
 "rwf" 'frameset-to-register
 "rwr" '(jump-to-register :which-key "restore-window-configuration")
 "rww" 'window-configuration-to-register

 ;;===================================================
 "s" '(:ignore t :which-key "search")
 "sq" '(vr/query-replace :which-key "query-replace")
 "sr" '(vr/replace :which-key "replace")
 "sa" 'helm-do-ag

 ;;===================================================
 "t" '(:ignore t :which-key "text")
 "t TAB" 'indent-rigidly
 "tc"    'count-words
 "tU"    'upcase-dwim
 "tu"    'downcase-dwim
 "ti"    '(ajw/autoformat-buffer :which-key "format-buffer")
 "tw"    'whitespace-mode

 "td" '(:ignore t :which-key "dos")
 "tdh" '(ajw/toggle-dos-eol :which-key "hide-dos-eol")
 "tds" '((lambda () (interactive) (ajw/toggle-dos-eol 1)) :which-key "show-dos-eol")

 "ta" '(:ignore t :which-key "align")
 "taa" 'align
 "tac" 'align-current
 "tae" 'align-entire
 "tar" 'align-regexp

 "tj" '(:ignore t :which-key "justify")
 "tjc" 'set-justification-center
 "tjf" 'set-justification-full
 "tjl" 'set-justification-left
 "tjn" 'set-justification-none
 "tjr" 'set-justification-right

 "tl" '(:ignore t :which-key "lines")
 "tlc" '(spacemacs/sort-lines-by-column :which-key "sort-lines-by-column")
 "tlC" '(spacemacs/sort-lines-by-column-reverse :which-key "sort-lines-by-column-reverse")
 "tld" '(spacemacs/duplicate-line-or-region :which-key "duplicate-line-or-region")
 "tls" '(spacemacs/sort-lines :which-key "sort-lines")
 "tlS" '(spacemacs/sort-lines-reverse :which-key "sort-lines-reverse")
 "tlu" '(spacemacs/uniquify-lines :which-key "uniquify-lines")

 "tt" '(:ignore t :which-key "transpose")
 "ttc" 'transpose-chars
 "ttl" 'transpose-lines
 "ttw" 'transpose-words

 "tr"  'query-replace

 ;;===================================================
 "w" '(:ignore t :which-key "windows")
 "w TAB" '(spacemacs/alternate-window :which-key "alternate-window")
 "w2"    '(spacemacs/layout-double-columns :which-key "layout-double-columns")
 "w3"    '(spacemacs/layout-triple-columns :which-key "layout-triple-columns")
 "wb"    '(spacemacs/switch-to-minibuffer-window :which-key "switch-to-minibuffer-window")
 "wm"    '(spacemacs/switch-to-minibuffer-window :which-key "switch-to-minibuffer-window")
 "wd"    'delete-window
 "wD"    'delete-other-windows
 "wf"    'follow-mode
 "wF"    'make-frame
 "wH"    'evil-window-move-far-left
 "w <S-left>"  'evil-window-move-far-left
 "wh"    'evil-window-left
 "w <left>"  'evil-window-left
 "wJ"    'evil-window-move-very-bottom
 "w <S-down>"  'evil-window-move-very-bottom
 "wj"    'evil-window-down
 "w <down>"  'evil-window-down
 "wK"    'evil-window-move-very-top
 "w <S-up>"  'evil-window-move-very-top
 "wk"    'evil-window-up
 "w <up>"  'evil-window-up
 "wL"    'evil-window-move-far-right
 "w <S-right>"  'evil-window-move-far-right
 "wl"    'evil-window-right
 "w <right>"  'evil-window-right
 "wo"    'other-window
 "wr"    '(spacemacs/rotate-windows-forward :which-key "rotate-windows-forward")
 "wR"    '(spacemacs/rotate-windows-backward :which-key "rotate-windows-backward")
 "wS"    'split-window-below
 "w-"    'split-window-below
 "ws"    '(ajw/split-window-below-and-focus :which-key "split-window-below-and-focus")
 "wV"    'split-window-right
 "wv"    '(ajw/split-window-right-and-focus :which-key "split-window-right-and-focus")
 "w/"    'split-window-right
 "w="    'balance-windows
 "w+"    '(spacemacs/window-layout-toggle :which-key "window-layout-toggle")
 )



;; Prog-mode specific keybindings
(ajw/general-prog-mode-SPC-leader

 "c" '(:ignore t :which-key "comment")
 "ca" '(ajw/append-comment-eol :which-key "append-comment-eol")
 "cI" '(ajw/insert-comment :which-key "insert-comment")
 "c/" '(ajw/comment-dwim :which-key "comment-dwim")
 "c;" '(ajw/comment-dwim :which-key "comment-dwim")
 "cl" '(ajw/comment-dwim :which-key "comment-dwim")
 "cu" 'uncomment-region
 "cf" '(ajw/autoformat-function :which-key "format-function")
 )

;; Compilation mode
(general-define-key
 :keymaps 'compilation-mode-map
 "j"   'compilation-next-error
 "k"   'compilation-previous-error
 "TAB" 'compilation-display-error
 "q"   'delete-window
 "SPC" nil
 )
