;;---------------------------------------------------------------
;; Built-in emacs configurations
;;---------------------------------------------------------------
;; Set default font
(when (member "IBM Plex Mono" (font-family-list))
  (set-face-attribute 'default nil :family "IBM Plex Mono" :slant 'normal :weight 'normal :height 98 :width 'normal))

;;Turn off toolbar
(tool-bar-mode -1)
(setq tool-bar-style 'both-horiz) ;;Image + text when toolbar mode is enabled

;;Turn off scrollbar
(scroll-bar-mode -1)

;;Turn off menu bar
(menu-bar-mode -1)

;;Don't overwrite minibuffer with messages when active
(define-advice message (:around (f &rest args) dont-disturb-in-mini)
  (let ((inhibit-message (or inhibit-message
                             (> (minibuffer-depth) 0))))
    (apply f args)))

;;Recursively add "lisp" directory to load path
(let ((default-directory (concat user-emacs-directory "lisp")))
  (normal-top-level-add-to-load-path '("."))
  (normal-top-level-add-subdirs-to-load-path))

;;Put backup files and auto-saves in /tmp
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;;Change "yes or no" prompts to "y or n"
(defalias 'yes-or-no-p 'y-or-n-p)

;;Disable Emacs startup message and scratch message
(setq inhibit-startup-message t
      initial-scratch-message nil)

;;Set custom startup echo message
(defun display-startup-echo-area-message ()
  (message "Dodongo dislikes smoke."))

;;Set line-numbers for programming only
;; (setq display-line-numbers-type 'relative)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;;Set visual-line-mode on for text modes only
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)

;;Set sentences to end with single space (for M-a and M-e)
(setq sentence-end-double-space nil)

;;Show matching parentheses
(setq show-paren-delay 0)
(show-paren-mode t)

;;Turn on bracket auto-pair
(electric-pair-mode 1)

;;Set up GDB
(setq gdb-display-io-nopopup t)
(setq gud-gdb-command-name "gdb -i=mi -nx")
(setq gdb-many-windows t)
(setq gdb-show-main t)

;;Overwrite highlighted area
(delete-selection-mode t)

;;Turn on line column numbering
(column-number-mode t)

;;Disable beep
(setq ring-bell-function 'ignore)

;;Enable blinking cursor
(blink-cursor-mode t)

;;Tramp ssh is faster than scp
(setq tramp-default-method "ssh")

;;Turn on line highlighting for programming
(add-hook 'prog-mode-hook 'hl-line-mode)

;;Put current file name in title bar
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;;Open Arch PKGBUILDs in shell-script-mode
(setq auto-mode-alist (append '(("/PKGBUILD$" . shell-script-mode)) auto-mode-alist))

;;Open Kbuild files in makefile-mode
(setq auto-mode-alist (append '(("/Kbuild$" . makefile-mode)) auto-mode-alist))

;;Change line length from 80
(setq-default fill-column 100 )
(setq-default whitespace-line-column 100)

;; Don't add ELPA files to recentf list when updating packages
(with-eval-after-load "recentf"
  (add-to-list 'recentf-exclude
	       (expand-file-name (concat user-emacs-directory "elpa/.*"))))

;; Don't truncate imenu entries
(setq imenu-max-item-length nil)

;; Set location
(setq calendar-location-name "Chicago, IL")
(setq calendar-latitude 41.89)
(setq calendar-longitude -87.63)

;;org-mode configurations
(setq org-ellipsis "  "
      org-startup-indented      t
      org-hide-emphasis-markers t
      org-src-fontify-natively  t
      org-src-tab-acts-natively t)

;;CC-mode configurations ======
(add-hook 'c-mode-hook 'ajw/toggle-dos-eol)
(add-hook 'c++-mode-hook 'ajw/toggle-dos-eol)

(setq-default c-default-style "bsd")
(setq-default c-basic-offset 4)          ; Four spaces per indent
(setq-default indent-tabs-mode nil)      ; No tabs, only spaces for indenting
(c-set-offset 'case-label '+)            ; Indent case labels in switch statements
(setq-default comment-style 'extra-line) ; Only one pair of /* */ around comment block

;; Always ask for compile command
(setq-default compilation-read-command t)
