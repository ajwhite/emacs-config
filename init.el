;;ELPA
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")))
(package-initialize)

;; Load custom file
(setq custom-file "~/.emacs.d/custom.el")
(when (file-exists-p custom-file)
  (load custom-file))

;; Bootstrap 'use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Load external files
(load (concat user-emacs-directory "config/config"))
(load (concat user-emacs-directory "config/ajw"))
(load (concat user-emacs-directory "config/packages"))
(load (concat user-emacs-directory "config/modeline"))
(load (concat user-emacs-directory "config/spacemacs"))
(load (concat user-emacs-directory "config/keybindings"))
(load (concat user-emacs-directory "config/theme"))
(load (concat user-emacs-directory "config/programming/programming"))
(load (concat user-emacs-directory "config/programming/c-c++"))

;; Load system-specific file
(load (concat user-emacs-directory "system") t)

(ajw/enable-theme)
