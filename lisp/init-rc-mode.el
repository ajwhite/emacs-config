;;; init-rc-mode.el --- Major mode for Android init.rc files
;;;
;;; Author: Andy White
;;;

(defvar init-rc-indentation 4
  "Indent width for init-rc-mode.")

;; Macro to generate this regexp for every symbol in a list and append them to
;; one another:
;; "^[[:space:]]*SYMBOL\\_>\\|"
;; The trailing "\\|" is removed
(defmacro init-rc--generate-symbol-regexp (symbols)
  `(string-trim-right (let (temp-val)
                        (dolist (elt ,symbols temp-val)
                          (setq temp-val (concat temp-val "^[[:space:]]*" elt "\\_>\\|"))))
                      "\\\\|"))


;; Regexp to detect keywords
;; Used for font-lock and indentation
(setq init-rc--keywords-regexp
      (init-rc--generate-symbol-regexp
       '("import" "on" "service")))

;; Regexp to detect commands
;; Used for font-lock and indentation
(setq init-rc--commands-regexp
      (init-rc--generate-symbol-regexp
       '("bootchart" "chmod" "chown" "class_start" "class_start_post_data"
         "class_stop" "class_reset" "class_reset_post_data" "class_restart"
         "copy" "domainname" "enable" "exec" "exec_background" "exec_start"
         "export" "hostname" "ifup" "insmod" "interface_start"
         "interface_restart" "interface_stop" "load_system_props" "loglevel"
         "mark_post_data" "mkdir" "mount_all" "mount" "parse_apex_configs"
         "restart" "restorecon" "restorecon_recursive" "rm" "rmdir" "readahead"
         "setprop" "setrlimit" "start" "stop" "swapon_all" "symlink" "sysclktz"
         "trigger" "umount" "verity_update_state" "wait" "wait_for_prop" "write")))

;; Regexp to detect service options
;; Used for font-lock and indentation
(setq init-rc--service-options-regexp
      (init-rc--generate-symbol-regexp
       '("capabilities" "class" "console" "critical" "disabled" "enter_namespace"
         "file" "group" "interface" "ioprio" "keycodes" "memcg\\.limit_in_bytes"
         "memcg\\.limit_property" "memcg\\.soft_limit_in_bytes" "memcg\\.swapiness"
         "namespace" "oneshot" "onrestart" "oom_score_adjust" "override" "priority"
         "reboot_on_failure" "restart_period" "rlimit" "seclabel" "setenv"
         "shutdown" "sigstop" "socket" "stdio_to_kmsg" "timeout_period" "updatable"
         "user" "writepid")))

(setq init-rc-font-lock-keywords
      (let ((property-regexp "${.*?}"))

        `((,init-rc--keywords-regexp . font-lock-keyword-face)
          (,init-rc--commands-regexp . font-lock-function-name-face)
          (,init-rc--service-options-regexp . font-lock-type-face)
          (,property-regexp . font-lock-variable-name-face))))


;; Match a regexp in the current line only. Returns nil if no-match, and the
;; position of the match otherwise
(defmacro init-rc--indent-match (regexp)
  `(re-search-forward ,regexp (point-at-eol) t))

(defun init-rc--calculate-indent ()
  "Calculate the indent offset for init-rc-mode using the following rules:

    1. Keywords should be indented zero levels.

    2. Commands and service options should be indented one level.

    3. Comments should be indented the same as the immediately following keyword,
       command, or service option and left alone otherwise. nil will be returned
       in the \"otherwise\" case.

    4. Anything not a keyword, command, service option, or comment should just
       be indented one level by default."
  (save-excursion
    (beginning-of-line)
    (cond
     ((init-rc--indent-match "^[[:space:]]*#")                   ; Comment
      ;; For comments, try to recursively find the next valid line and indent
      ;; to that level.
      (let ((current-line (line-number-at-pos))
            (last-line (line-number-at-pos (point-max))))
        (if (eql current-line last-line) ; Return nil if we're on the last line
            nil
          (forward-line)
          (if (init-rc--indent-match "^[[:space:]]*$") ; Also return nil if next line is whitespace
              nil
            (init-rc--calculate-indent)))))
     ((init-rc--indent-match init-rc--keywords-regexp)        0) ; Keyword
     ((init-rc--indent-match init-rc--commands-regexp)        1) ; Command
     ((init-rc--indent-match init-rc--service-options-regexp) 1) ; Service option
     (t 1))))



(defun init-rc-indent-line ()
  (interactive)
  (let ((indent (init-rc--calculate-indent)))
    (when indent
      (save-excursion
        (indent-line-to (* indent init-rc-indentation))))
    ;; If at beginning of line, move to first non-whitespace character
    (when (or (bolp) (looking-back "^[[:space:]]+"))
      (beginning-of-line-text))))

;;;###autoload
(define-derived-mode init-rc-mode shell-script-mode "init.rc (Android)"
  "Major mode for editing Android init.rc files."

  (set (make-local-variable 'font-lock-defaults) '((init-rc-font-lock-keywords)))
  (set (make-local-variable 'indent-line-function) 'init-rc-indent-line))

;;;###autoload
(add-to-list 'auto-mode-alist '("/init\\(\\..+?\\)*?\\.rc\\'" . init-rc-mode)) ; basename =~ /^init(\..+?)*?\.rc$/

(provide 'init-rc-mode)
