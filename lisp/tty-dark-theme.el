;;; tty-dark-theme.el --- tty-dark theme

;; Copyright (C) 2001 by Oivvio Polite
;; Copyright (C) 2013 by Syohei YOSHIDA

;; Author: Syohei YOSHIDA <syohex@gmail.com>
;; URL: https://github.com/emacs-jp/replace-colorthemes
;; Version: 0.01

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Port of tty-dark theme from `color-themes'

;; Edited by Andy White

;;; Code:

(deftheme tty-dark
  "tty-dark theme")

(custom-theme-set-faces
 'tty-dark

 '(default ((t (:background nil :foreground "#FFFFFF"))))
 '(mouse ((t (:foreground "brightblack"))))
 '(cursor ((t (:background "#FFFFFF"))))
 '(border ((t (:foreground "brightblue"))))

 '(bold ((t (:underline t :background nil :foreground "#FFFFFF"))))
 '(bold-italic ((t (:underline t :foreground "#FFFFFF"))))
 '(calendar-today-face ((t (:underline t))))
 '(diary-face ((t (:foreground "brightred"))))
 '(font-lock-builtin-face ((t (:foreground "brightblue"))))
 '(font-lock-comment-face ((t (:foreground "brightblue"))))
 '(font-lock-constant-face ((t (:foreground "brightmagenta"))))
 '(font-lock-function-name-face ((t (:foreground "brightblue"))))
 '(font-lock-keyword-face ((t (:foreground "brightred"))))
 '(font-lock-string-face ((t (:foreground "brightgreen"))))
 '(font-lock-type-face ((t (:foreground "brightyellow"))))
 '(font-lock-variable-name-face ((t (:foreground "brightblue"))))
 '(font-lock-warning-face ((t (:bold t :foreground "brightmagenta"))))
 '(highlight ((t (:background "brightblue" :foreground "brightyellow"))))
 '(holiday-face ((t (:background "brightcyan"))))
 '(info-menu-5 ((t (:underline t))))
 '(info-node ((t (:italic t :bold t))))
 '(info-xref ((t (:bold t))))
 '(italic ((t (:underline t :background nil))))
 '(message-cited-text-face ((t (:foreground "brightred"))))
 '(message-header-cc-face ((t (:bold t :foreground "brightgreen"))))
 '(message-header-name-face ((t (:foreground "brightgreen"))))
 '(message-header-newsgroups-face ((t (:italic t :bold t :foreground "brightyellow"))))
 '(message-header-other-face ((t (:foreground "#b00000"))))
 '(message-header-subject-face ((t (:foreground "brightgreen"))))
 '(message-header-to-face ((t (:bold t :foreground "brightgreen"))))
 '(message-header-xheader-face ((t (:foreground "brightblue"))))
 '(message-mml-face ((t (:foreground "brightgreen"))))
 '(message-separator-face ((t (:foreground "brightblue"))))

 '(mode-line ((t (:background "brightblack" :foreground "brightblue" :bold t))))
 '(mode-line-buffer-id ((t (:background nil :foreground "brightred"))))
 '(mode-line-mousable ((t (:background "#FFFFFF" :foreground "brightmagenta"))))
 '(mode-line-mousable-minor-mode ((t (:background "#FFFFFF" :foreground "brightyellow"))))
 '(hl-line ((t (:background "brightblack" :extend t))))
 '(region ((t (:background "#FFFFFF" :foreground "brightblack" :extend t))))
 '(zmacs-region ((t (:background "brightcyan" :foreground "brightblack"))))
 '(secondary-selection ((t (:background "brightblue"))))
 '(show-paren-match-face ((t (:background "brightred"))))
 '(show-paren-mismatch-face ((t (:background "brightmagenta" :foreground "#FFFFFF"))))
 '(underline ((t (:underline t))))

 '(lsp-face-highlight-textual ((t (:background "white" :distant-foreground "black" :bold t))))
 '(lsp-face-highlight-read ((t (:background "brightred" :underline t :bold t))))
 '(lsp-face-highlight-write ((t (:background "brightgreen" :italic t :bold t))))
 )


;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'tty-dark)

;;; tty-dark-theme.el ends here
