;;; breeze-dark-theme.el --- breeze-dark theme
;;;
;;; Attempt at using KDE's Breeze-Dark color scheme in an Emacs theme
;;;
;;; Author: Andy White

(deftheme breeze-dark
  "breeze-dark theme")

(let ((color1 "#232627")
      (color2 "#ED1515")
      (color3 "#11D116")
      (color4 "#F67400")
      (color5 "#1D99F3")
      (color6 "#9B59B6")
      (color7 "#1ABC9C")
      (color8 "#FCFCFC")

      (color1-bright "#7F8C8D")
      (color2-bright "#C0392B")
      (color3-bright "#1CDC9A")
      (color4-bright "#FDBC4B")
      (color5-bright "#3DAEE9")
      (color6-bright "#8E44AD")
      (color7-bright "#16A085")
      (color8-bright "#FFFFFF")

      (color1-mid "#333333"))           ; gray20

  (custom-theme-set-faces
   'breeze-dark

   `(default ((t (:background ,color1 :foreground ,color8))))
   `(mouse ((t (:foreground ,color1-bright))))
   `(cursor ((t (:background ,color8-bright))))
   `(border ((t (:foreground ,color5-bright))))
   `(fringe ((t (:background ,color1))))
   `(link ((t (:foreground ,color5-bright :underline t))))
   `(link-visited ((t (:foreground ,color5-bright :underline nil))))

   `(success ((t (:foreground ,color3-bright))))
   `(warning ((t (:foreground ,color4-bright))))
   `(error   ((t (:foreground ,color2-bright))))

   `(bold ((t (:underline t :background nil :foreground ,color8-bright))))
   `(bold-italic ((t (:underline t :foreground ,color8-bright))))
   '(calendar-today-face ((t (:underline t))))
   `(diary-face ((t (:foreground ,color2-bright))))

   `(font-lock-builtin-face ((t (:foreground ,color5-bright))))
   `(font-lock-comment-face ((t (:foreground ,color5-bright))))
   `(font-lock-constant-face ((t (:foreground ,color6-bright))))
   `(font-lock-function-name-face ((t (:foreground ,color5-bright))))
   `(font-lock-keyword-face ((t (:foreground ,color2-bright))))
   `(font-lock-string-face ((t (:foreground ,color3-bright))))
   `(font-lock-type-face ((t (:foreground ,color4-bright))))
   `(font-lock-variable-name-face ((t (:foreground ,color5-bright))))
   `(font-lock-warning-face ((t (:bold t :foreground ,color6-bright))))

   `(highlight ((t (:background ,color5-bright :foreground ,color4-bright))))
   `(holiday-face ((t (:background ,color7-bright))))
   '(info-menu-5 ((t (:underline t))))
   '(info-node ((t (:italic t :bold t))))
   '(info-xref ((t (:bold t))))
   '(italic ((t (:underline t :background nil))))
   `(message-cited-text-face ((t (:foreground ,color2-bright))))
   `(message-header-cc-face ((t (:bold t :foreground ,color3-bright))))
   `(message-header-name-face ((t (:foreground ,color3-bright))))
   `(message-header-newsgroups-face ((t (:italic t :bold t :foreground ,color4-bright))))
   `(message-header-other-face ((t (:foreground ,color2-bright))))
   `(message-header-subject-face ((t (:foreground ,color3-bright))))
   `(message-header-to-face ((t (:bold t :foreground ,color3-bright))))
   `(message-header-xheader-face ((t (:foreground ,color5-bright))))
   `(message-mml-face ((t (:foreground ,color3-bright))))
   `(message-separator-face ((t (:foreground ,color5-bright))))

   `(mode-line ((t (:background ,color1 :foreground ,color5-bright :bold t :box (:line-width 1 :color ,color5-bright)))))
   `(mode-line-inactive ((t (:background ,color1-mid :foreground ,color1-bright :bold t :box (:line-width 1 :color ,color5)))))
   `(mode-line-buffer-id ((t (:background nil :foreground ,color2))))
   `(mode-line-mousable ((t (:background ,color8-bright :foreground ,color6-bright))))
   `(mode-line-mousable-minor-mode ((t (:background ,color8-bright :foreground ,color4-bright))))

   `(minibuffer-prompt ((t (:foreground ,color5-bright))))

   `(region  ((t (:background ,color1-mid :foreground nil :extend t))))
   `(hl-line ((t (:inherit region))))

   `(secondary-selection ((t (:background ,color5-bright))))
   `(show-paren-match-face ((t (:background ,color2-bright))))
   `(show-paren-mismatch-face ((t (:background ,color6-bright :foreground ,color8-bright))))
   '(underline ((t (:underline t))))

   ;; TODO
   ;; compilation messages (also used by several other modes)

   ;; MODE SUPPORT: diff
   `(diff-refine-removed ((t (:background ,color2-bright))))
   `(diff-refine-added   ((t (:background ,color3-bright))))

   ;; MODE SUPPORT: which-key
   `(which-key-key-face               ((t (:foreground ,color5-bright :bold t))))
   `(which-key-group-description-face ((t (:foreground ,color2 :bold t))))

   ;; MODE SUPPORT: man/woman
   `(Man-overstrike ((t (:foreground ,color4-bright :bold t))))
   `(Man-underline  ((t (:foreground ,color7-bright :bold t))))
   `(woman-bold     ((t (:inherit Man-overstrike))))
   `(woman-italic   ((t (:inherit Man-underline))))

   ;; MODE SUPPORT: whitespace-mode
   ;; MODE SUPPORT: company
   `(company-echo-common              ((t (:foreground ,color5-bright :background nil))))
   `(company-echo                     ((t (:inherit 'company-echo-common))))
   `(company-template-field           ((t (:foreground ,color5-bright :background nil :underline nil))))
   `(company-scrollbar-fg             ((t (:foreground nil :background ,color5-bright))))
   `(company-scrollbar-bg             ((t (:foreground nil :background ,color1-mid))))
   `(company-tooltip                  ((t (:foreground ,color8 :background ,color1))))
   `(company-preview-common           ((t (:inherit 'font-lock-comment-face))))
   `(company-tooltip-common           ((t (:foreground ,color3-bright))))
   `(company-tooltip-annotation       ((t (:foreground ,color6-bright))))
   `(company-tooltip-selection        ((t (:foreground ,color8 :background ,color1-mid))))

   ;; MODE SUPPORT: helm
   `(helm-selection         ((t (:foreground ,color8 :background ,color1-mid :extend t))))
   `(helm-selection-line    ((t (:inherit helm-selection))))
   `(helm-match             ((t (:foreground ,color8 :background ,color5-bright :bold t))))
   `(helm-candidate-number  ((t (:foreground ,color1 :background ,color4-bright :bold t))))
   `(helm-source-header     ((t (:foreground ,color8 :background ,color7-bright :bold t :underline t :extend t))))

   ;; MODE SUPPORT: helm-xref
   `(helm-xref-file-name   ((t (:foreground ,color3-bright))))
   `(helm-xref-line-number ((t (:inherit helm-xref-file-name))))

   ;; MODE SUPPORT: magit
   `(magit-section-highlight            ((t (:background ,color1-mid))))
   `(magit-branch                       ((t (:foreground ,color5-bright :box (:line-width 1 :color ,color5-bright)))))
   '(magit-branch-local                 ((t (:inherit magit-branch))))
   `(magit-branch-remote                ((t (:foreground ,color3-bright :bold t))))
   ;; `(magit-cherry-equivalent            ((t (:foreground darktooth-neutral_orange))))
   ;; `(magit-cherry-unmatched             ((t (:foreground darktooth-neutral_purple))))
   ;; `(magit-diff-context                 ((t (:foreground darktooth-dark3 :background nil))))
   ;; `(magit-diff-context-highlight       ((t (:foreground darktooth-dark4 :background darktooth-dark0_soft))))
   `(magit-diff-added                   ((t (:background ,color7-bright))))
   `(magit-diff-added-highlight         ((t (:inherit magit-diff-added))))
   `(magit-diff-removed                 ((t (:inherit diff-refine-removed))))
   `(magit-diff-removed-highlight       ((t (:inherit magit-diff-removed))))
   `(magit-diff-add                     ((t (:foreground ,color7-bright))))
   `(magit-diff-del                     ((t (:foreground ,color2-bright))))
   ;; `(magit-diff-file-header             ((t (:foreground darktooth-bright_blue))))
   ;; `(magit-diff-hunk-header             ((t (:foreground darktooth-neutral_aqua))))
   ;; `(magit-diff-merge-current           ((t (:background darktooth-dark_yellow))))
   ;; `(magit-diff-merge-diff3-separator   ((t (:foreground darktooth-neutral_orange :weight 'bold))))
   ;; `(magit-diff-merge-proposed          ((t (:background darktooth-dark_green))))
   ;; `(magit-diff-merge-separator         ((t (:foreground darktooth-neutral_orange))))
   ;; `(magit-diff-none                    ((t (:foreground darktooth-medium))))
   ;; `(magit-item-highlight               ((t (:background darktooth-dark1 :weight 'normal))))
   ;; `(magit-item-mark                    ((t (:background darktooth-dark0))))
   ;; `(magit-key-mode-args-face           ((t (:foreground darktooth-light4))))
   ;; `(magit-key-mode-button-face         ((t (:foreground darktooth-neutral_orange :weight 'bold))))
   ;; `(magit-key-mode-header-face         ((t (:foreground darktooth-light4 :weight 'bold))))
   ;; `(magit-key-mode-switch-face         ((t (:foreground darktooth-turquoise4 :weight 'bold))))
   ;; `(magit-log-author                   ((t (:foreground darktooth-neutral_aqua))))
   ;; `(magit-log-date                     ((t (:foreground darktooth-faded_orange))))
   ;; `(magit-log-graph                    ((t (:foreground darktooth-light1))))
   ;; `(magit-log-head-label-bisect-bad    ((t (:foreground darktooth-bright_red))))
   ;; `(magit-log-head-label-bisect-good   ((t (:foreground darktooth-bright_green))))
   ;; `(magit-log-head-label-bisect-skip   ((t (:foreground darktooth-neutral_yellow))))
   ;; `(magit-log-head-label-default       ((t (:foreground darktooth-neutral_blue))))
   ;; `(magit-log-head-label-head          ((t (:foreground darktooth-light0 :background darktooth-dark_aqua))))
   ;; `(magit-log-head-label-local         ((t (:foreground darktooth-faded_blue :weight 'bold))))
   ;; `(magit-log-head-label-patches       ((t (:foreground darktooth-faded_orange))))
   ;; `(magit-log-head-label-remote        ((t (:foreground darktooth-neutral_blue :weight 'bold))))
   ;; `(magit-log-head-label-tags          ((t (:foreground darktooth-neutral_aqua))))
   ;; `(magit-log-head-label-wip           ((t (:foreground darktooth-neutral_red))))
   ;; `(magit-log-message                  ((t (:foreground darktooth-light1))))
   ;; `(magit-log-reflog-label-amend       ((t (:foreground darktooth-bright_blue))))
   ;; `(magit-log-reflog-label-checkout    ((t (:foreground darktooth-bright_yellow))))
   ;; `(magit-log-reflog-label-cherry-pick ((t (:foreground darktooth-neutral_red))))
   ;; `(magit-log-reflog-label-commit      ((t (:foreground darktooth-neutral_green))))
   ;; `(magit-log-reflog-label-merge       ((t (:foreground darktooth-bright_green))))
   ;; `(magit-log-reflog-label-other       ((t (:foreground darktooth-faded_red))))
   ;; `(magit-log-reflog-label-rebase      ((t (:foreground darktooth-bright_blue))))
   ;; `(magit-log-reflog-label-remote      ((t (:foreground darktooth-neutral_orange))))
   ;; `(magit-log-reflog-label-reset       ((t (:foreground darktooth-neutral_yellow))))
   ;; `(magit-log-sha1                     ((t (:foreground darktooth-bright_orange))))
   ;; `(magit-process-ng                   ((t (:foreground darktooth-bright_red :weight 'bold))))
   ;; `(magit-process-ok                   ((t (:foreground darktooth-bright_green :weight 'bold))))
   `(magit-section-heading              ((t (:foreground ,color5-bright :bold t))))
   ;; `(magit-signature-bad                ((t (:foreground darktooth-bright_red :weight 'bold))))
   ;; `(magit-signature-good               ((t (:foreground darktooth-bright_green :weight 'bold))))
   ;; `(magit-signature-none               ((t (:foreground darktooth-faded_red))))
   ;; `(magit-signature-untrusted          ((t (:foreground darktooth-bright_purple :weight 'bold))))
   ;; `(magit-tag                          ((t (:foreground darktooth-darkslategray4))))
   ;; `(magit-whitespace-warning-face      ((t (:background darktooth-faded_red))))
   ;; `(magit-bisect-bad                   ((t (:foreground darktooth-faded_red))))
   ;; `(magit-bisect-good                  ((t (:foreground darktooth-neutral_green))))
   ;; `(magit-bisect-skip                  ((t (:foreground darktooth-light2))))
   ;; `(magit-blame-date                   ((t (:inherit 'magit-blame-heading))))
   ;; `(magit-blame-name                   ((t (:inherit 'magit-blame-heading))))
   ;; `(magit-blame-hash                   ((t (:inherit 'magit-blame-heading))))
   ;; `(magit-blame-summary                ((t (:inherit 'magit-blame-heading))))
   ;; `(magit-blame-heading                ((t (:background darktooth-dark1 :foreground darktooth-light0))))
   ;; `(magit-sequence-onto                ((t (:inherit 'magit-sequence-done))))
   ;; `(magit-sequence-done                ((t (:inherit 'magit-hash))))
   ;; `(magit-sequence-drop                ((t (:foreground darktooth-faded_red))))
   ;; `(magit-sequence-head                ((t (:foreground darktooth-faded_cyan))))
   ;; `(magit-sequence-part                ((t (:foreground darktooth-bright_yellow))))
   ;; `(magit-sequence-stop                ((t (:foreground darktooth-bright_aqua))))
   ;; `(magit-sequence-pick                ((t (:inherit 'default))))
   ;; `(magit-filename                     ((t (:weight 'normal))))
   ;; `(magit-refname-wip                  ((t (:inherit 'magit-refname))))
   ;; `(magit-refname-stash                ((t (:inherit 'magit-refname))))
   ;; `(magit-refname                      ((t (:foreground darktooth-light2))))
   ;; `(magit-head                         ((t (:inherit 'magit-branch-local))))
   ;; `(magit-popup-disabled-argument      ((t (:foreground darktooth-light4))))

   ;; MODE SUPPORT: lsp
   `(lsp-face-highlight-textual ((t (:background ,color5-bright :distant-foreground ,color1 :bold t))))
   `(lsp-face-highlight-read    ((t (:foreground nil :background nil :inherit 'diff-refine-removed :bold t))))
   `(lsp-face-highlight-write   ((t (:foreground nil :background nil :inherit 'diff-refine-added :bold t))))
   ))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'breeze-dark)
